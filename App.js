import Layout from "./src/layout/LayoutDeTelaEstrutura";
import LayoutHorizontal from "./src/layout/LayoutHorizontal";
import LayoutGrade from "./src/layout/LayoutGrade";
import Component from "./src/component/Component";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import TelaInicial from "./src/TelaInicial";
import SegundaTela from "./src/SegundaTela";
import Menu from "./src/component/Menu";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Menu" component={Menu}/>
        <Stack.Screen name="TelaInicial" component={TelaInicial}/>
        <Stack.Screen name="SegundaTela" component={SegundaTela}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}
