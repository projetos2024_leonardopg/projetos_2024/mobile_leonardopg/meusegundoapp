import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View, ScrollView } from "react-native";

export default function App() {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text>Menu</Text>
      </View>
      <View style={styles.content}>
        <ScrollView>
          <Text>Conteudo</Text>
        </ScrollView>
      </View>
      <View style={styles.footer}>
        <Text>Rodape</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
  },
  header: {
    height: 50,
    backgroundColor: "blue",
  },
  content: {
    flex: 1,
    backgroundColor: "green",
  },
  footer: {
    height: 50,
    backgroundColor: "red",
  },
});
