import { StatusBar } from "expo-status-bar";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import Layout from "../layout/LayoutDeTelaEstrutura";
import LayoutHorizontal from "../layout/LayoutHorizontal";
import LayoutGrade from "../layout/LayoutGrade";
import Component from "./Component";

export default function Menu({navigation}) {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.menu}
        onPress={() => navigation.navigate("TelaInicial")}
      >
        <Text>Opção 1</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.menu}
        onPress={() => navigation.navigate("SegundaTela")}
      >
        <Text>Opção 2</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.menu}>
        <Text>Opção 3</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  menu: {
    padding: 10,
    margin: 5,
    backgroundColor: "lightblue",
    borderRadius: 5,
  },
});
